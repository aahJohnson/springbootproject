package com.example.springbootproject;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binomialName;
}
