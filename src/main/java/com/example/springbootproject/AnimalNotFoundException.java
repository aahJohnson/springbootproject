package com.example.springbootproject;

public class AnimalNotFoundException extends Exception {
    public AnimalNotFoundException(String id) {
        super(id);
    }
}
