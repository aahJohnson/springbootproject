package com.example.springbootproject;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/animals")
@AllArgsConstructor
public class AnimalController {

    AnimalService animalService;

    @GetMapping
    public List<Animal> all() {
        return List.of(
                new Animal(UUID.randomUUID().toString(), "Cat", "C", "", ""),
                new Animal(UUID.randomUUID().toString(), "Dog", "D", "", "")
        );
    }

    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal) {
        return new Animal(
                UUID.randomUUID().toString(),
                createAnimal.getName(),
                createAnimal.getBinomialName(),
                "",
                ""
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Animal> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(animalService.get(id)));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Animal> update(@PathVariable("id") String id, @RequestBody UpdateAnimal updateAnimal) {
        try {
            return ResponseEntity.ok(toDTO(animalService.updateAnimal(id, updateAnimal.getName(), updateAnimal.getBinomialName())));
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            animalService.delete(id);
            return ResponseEntity.ok().build();
        } catch (AnimalNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    private static Animal toDTO(AnimalEntity animalEntity) {
        return new Animal(
                animalEntity.getId(),
                animalEntity.getName(),
                animalEntity.getBinomialName(),
                animalEntity.getDescription(),
                animalEntity.getConservationStatus()
        );
    }
}