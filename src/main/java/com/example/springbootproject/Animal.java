package com.example.springbootproject;
import lombok.Value;

@Value
public class Animal {
    String id;
    String name;
    String binomialName;
    String description;
    String conservationStatus;
}
