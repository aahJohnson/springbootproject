package com.example.springbootproject;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
